<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ejemplo Angular</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body ng-app="myApp" ng-controller="myController">
	<div class="container-fluid">
		<div class="container">
			<div id="base-url" class="hide">
				<?php echo base_url(); ?>
			</div>
			<h1>
				Formulario con services
			</h1>

			<form name="nombreFormulario" id="nombreFormulario">
				<div class="form-group">
					<label for="mail">Correo</label>
					<input type="email" class="form-control" id="mail" name="mail" ng-model="Formulario.mail" placeholder="Correo" required>
				</div>
				<div class="form-group">
					<label for="exampleInputPassword1">Nombre</label>
					<input type="text" class="form-control" id="mail" name="mail" ng-model="Formulario.nombre" placeholder="Nombre" required>
				</div>
				<div class="form-group">
					<label for="exampleInputFile">Departamento</label>
					<select class="form-control" id="departamento" name="departamento" 
					ng-model="Formulario.departamento" 
					ng-change="getmunicipios({'id':Formulario.departamento})" required>
					<option ng-repeat="departamento in departamentos" value="{{departamento.id}}">{{departamento.nombre}}</option>
				</select>
			</div>
			<div class="form-group">
				<label for="exampleInputFile">Municipio</label>
				<select class="form-control" id="municipio" name="municipio" ng-model="Formulario.municipio" required>
					<option ng-repeat="municipio in municipios" value="{{municipio.id}}">{{municipio.nombre}}</option>
				</select>
			</div>
			<button type="submit" class="btn btn-primary">Enviar</button>
		</form>
	</div>
</div>
</body>
<!-- Jquery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

<!-- Boopttrap -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- AngularJS -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>

<!-- My JS Remplazar con la ruta propia usando el metodo de en php base_url() -->
<script src="<?php echo base_url("/assets/js/basicos/angular-services1.js"); ?>" rel="stylesheet"></script>
</html>