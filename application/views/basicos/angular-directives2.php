<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Directivas Angular</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body ng-app="myApp" ng-controller="myController">
	<div class="container-fluid">
		<div class="container">
			<div id="base-url" class="hide">
				<?php echo base_url(); ?>
			</div>
			<h1>
				Directivas
			</h1>
			<div test-template></div>
			<br>
			<div test-parametros="{value:'Valor como parametro'}"></div>
			<br>
			<div test-funcion="algunValor" ng-model="algunValor"></div>
			<br>
			<div>
				En el controller {{algunValor}}
			</div>
			
			<br>
			<div test-funcion2 ng-model="algunOtroValor" test-value="algunOtroValor"></div>
			<br>
			<div>
				En el controller {{algunOtroValor}}
			</div>
	</div>
</div>
</body>
<!-- Jquery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

<!-- Boopttrap -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- AngularJS -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
	
<!-- My JS Remplazar con la ruta propia usando el metodo de en php base_url() -->
<script src="<?php echo base_url("/assets/js/basicos/angular-directives2.js"); ?>" rel="stylesheet"></script>
</html>