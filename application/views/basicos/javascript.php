<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ejemplo JavaScript</title>
</head>
<body>
	<h1>
    JavaScript
  </h1>
	<a class="btn btn-primary" href="<?php echo base_url("/curso/basicos/"); ?>" role="button">Volver</a>
	<br/><br/>
	<div>
		Ingrese nombre:
		<input type="text" name="nombre" id="nombre" onkeyup="updateOutput(this)"></input>
	</div>
	<div id="output">Hola</div>
</body>
<script src="<?php echo base_url("/assets/js/basicos/javascript.js"); ?>" rel="stylesheet"></script>
</html>
