<div class="container">
	<a class="btn btn-primary" href="<?php echo base_url(); ?>" role="button">Inicio</a>
	<a class="btn btn-default" href="<?php echo base_url("/curso/basicos/angular-directives1"); ?>" role="button">Derectivas basicas</a>
	<a class="btn btn-default" href="<?php echo base_url("/curso/basicos/angular-directives2"); ?>" role="button">Agregar elementos</a>
	<br>
	<br>
	<div id="base-url" class="hide">
		<?php echo base_url(); ?>
	</div>
</div>
<div class="container-fluid" id="viewApp" ng-app="viewCodeApp" ng-controller="directivasController">
	<div class="col-md-12">
		<div class="thumbnail">
			
			<md-content class="md-padding">
				<md-tabs md-selected="selectedIndex" md-dynamic-height md-border-bottom md-autoselect>
					<md-tab ng-repeat="tab in tabs" label="{{tab.title}}">
						<div class="demo-tab tabs{{$index%4}}">
							<div>
								<iframe ng-src="{{ tab.content }}" width="100%" frameborder="0" scrolling="auto" height="800"></iframe>
							</div>
							
						</div>
					</md-tab>
				</md-tabs>
			</md-content>
			
		</div>
	</div>
</div>