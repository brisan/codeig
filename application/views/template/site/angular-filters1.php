<div class="container">
		<a class="btn btn-primary" href="<?php echo base_url(); ?>" role="button">Inicio</a>
		<a class="btn btn-default" href="https://www.w3schools.com/angular/tryit.asp?filename=try_ng_filters_uppercase" target="_blank" role="button">Filtro mayuscula</a>
		<a class="btn btn-default" href="https://www.w3schools.com/angular/tryit.asp?filename=try_ng_filters_currency" target="_blank" role="button">Filtros Moneda</a>
	  <a class="btn btn-default" href="https://www.w3schools.com/angular/tryit.asp?filename=try_ng_filters_filter" target="_blank" role="button">Filtros Texto</a>	
		<a class="btn btn-default" href="https://www.w3schools.com/angular/tryit.asp?filename=try_ng_filters_orderby" target="_blank" role="button">Filtros de directivas</a>
	<a class="btn btn-default" href="	https://www.w3schools.com/angular/tryit.asp?filename=try_ng_filters_custom" target="_blank" role="button">Filtros propios</a>
	
		<br>
		<br>
		<div id="base-url" class="hide">
			<?php echo base_url(); ?>
		</div>
	</div>
<div class="container-fluid" id="viewApp" ng-app="viewCodeApp">
  	<div class="col-md-12">
			<legend>
				<h2>
					JS
				</h2>
			</legend>
			<pre class="language-javascript line-numbers" style="height:400px;" data-src="<?php echo base_url("/assets/js/basicos/angular-filters1.js"); ?>"></pre>
			<pre>
		<code>
		texto = "algun - texto - a - filtrar";
		texto | split:' - ':3
		</code>
		</pre>
	
	</div>
	
		</div>
	</div>