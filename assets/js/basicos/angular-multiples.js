var app = angular.module('myApp', []);

app.controller('myController', ['$scope', function($scope){
  console.log({"Controlador": "myController"});
  $scope.name = "Brian";
}]);

app.controller('cualquierNombre', ['$scope', function($scope){
  console.log({"Controlador": "cualquierNombre"});
  $scope.name = "Otro valor";
}]);